const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const PORT = 3006;

// connect routes to index.js file
const taskRoutes = require('./routes/taskRoutes');

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// Mongoose connection
	// mongoose.connect(<connection string>, {options})
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

// DB connection notification
	// use connection property of mongoose
const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))


// SCHEMA
	//moved the schema to models folder


// ROUTES
// http://localhost:3000/api/tasks
app.use(`/api/tasks`, taskRoutes)




app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))
